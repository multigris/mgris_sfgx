import numpy as np
import matplotlib
import sys
import os.path
import scipy as sc
from pylab import *
import matplotlib.pylab as plt
from astropy import table
import scipy.stats as Sis
from astropy.io import ascii
from pathlib import Path
import pandas as pd
import argparse


def get_stopping_cond(model_name):
    """Flag for models stopping at Av=10"""
    cond = np.where(summary["Model name"] == model_name)
    if summary["Stop"][cond] == "A_V reached.":
        return 1
    else:
        return 0


def Rin(dir_, model_name):
    out = open(dir_ + "/" + model_name + ".in", "r")
    data = [line.split() for line in out if "radius" in line.split()]
    return 10 ** float(data[0][1])


def dust_to_gas(dir_, model_name):
    out = open(dir_ + "/" + model_name + ".out", "r")
    data = [line for line in out if "Dust to gas ratio (by mass)" in line]
    dgr = float(data[0].split(",")[0].split(":")[-1])
    return dgr


def merge_fix_cols(df_merged):
    for col in df_merged:
        if col.endswith("_x"):
            df_merged.rename(columns=lambda col: col.rstrip("_x"), inplace=True)
        elif col.endswith("_y"):
            to_drop = [col for col in df_merged if col.endswith("_y")]
            df_merged.drop(to_drop, axis=1, inplace=True)
        else:
            pass
    return df_merged


def total_mass(df, depth_vals, X, R_in):
    total_mass = (
        depth_2_MH2(df, depth_vals, X, R_in, zone="total")
        + depth_2_MHI(df, depth_vals, X, R_in)
        + depth_2_MHII(df, depth_vals, X, R_in)
    )
    return total_mass


def Mdust(dir_, model_name, df, depth_vals, X, R_in):
    return dust_to_gas(dir_, model_name) * total_mass(df, depth_vals, X, R_in)


def depth_2_Av(df, depth_vals, X):
    Y = df["AV(extend)"]
    Av = np.interp(depth_vals, X, Y)
    return Av


def Av_2_depth(df, Av_vals, X):
    Y = X
    X = df["AV(extend)"]
    depth = np.interp(Av_vals, X, Y)
    return depth


def depth_2_colden(df, depth_vals, X):
    dr = np.array([0] + [X.iloc[i] - X.iloc[i - 1] for i in range(1, len(df))])
    Y = df["hden"] * dr
    colden = np.interp(depth_vals, X, Y)
    return colden


def depth_2_MH2(df, depth_vals, X, R_in, zone="total"):
    """ H2 mass (except for the total one) are weighted by the relative abundance profiles"""
    if zone == "total":
        mask = np.ones(len(df))
    elif zone == "CO":
        mask = df["CO/C"]  # np.array( df['CO/C']-0.95 > 0, dtype=float)
    elif zone == "C":
        mask = df["C1"]  # np.array( df['C1']-0.95 > 0, dtype=float)
    elif zone == "C+":
        mask = df["C2"]  # np.array( df['C2']-0.95 > 0, dtype=float)
    dV = np.array(
        [0]
        + [
            (4 / 3) * np.pi * ((R_in + X.iloc[i]) ** 3 - (R_in + X.iloc[i - 1]) ** 3)
            for i in range(1, len(df))
        ]
    )
    n_H2 = (1 / 2) * df["2H_2/H"] * df["hden"]
    Y = np.cumsum(n_H2 * dV * mask)
    M_H2 = np.interp(depth_vals, X, Y) * 2 * h_mass
    return M_H2


def depth_2_MHI(df, depth_vals, X, R_in):
    dV = np.array(
        [0]
        + [
            (4 / 3) * np.pi * ((R_in + X.iloc[i]) ** 3 - (R_in + X.iloc[i - 1]) ** 3)
            for i in range(1, len(df))
        ]
    )
    n_HI = df["HI"] * df["hden"]
    Y = np.cumsum(n_HI * dV)
    M_HI = np.interp(depth_vals, X, Y) * h_mass
    return M_HI


def depth_2_MHII(df, depth_vals, X, R_in):
    dV = np.array(
        [0]
        + [
            (4 / 3) * np.pi * ((R_in + X.iloc[i]) ** 3 - (R_in + X.iloc[i - 1]) ** 3)
            for i in range(1, len(df))
        ]
    )
    n_HII = df["HII"] * df["hden"]
    Y = np.cumsum(n_HII * dV)
    M_HII = np.interp(depth_vals, X, Y) * (h_mass - e_mass)
    return M_HII


def depth_2_CII(df, depth_vals, X, zone="total"):
    """ H2 mass (except for the total one) are weighted by the relative abundance profiles"""
    if zone == "total":
        mask = np.ones(len(df))
    elif zone == "H+":
        mask = df["HII"]  # np.array( df['CO/C']-0.95 > 0, dtype=float)
    elif zone == "H":
        mask = df["HI"]  # np.array( df['C1']-0.95 > 0, dtype=float)
    elif zone == "H2":
        mask = df["2H_2/H"]  # np.array( df['C2']-0.95 > 0, dtype=float)
    C2 = df["C  2 157.636m"].iloc[:, 0]
    Y = np.cumsum(C2 * mask)
    C2_zone = np.interp(depth_vals, X, Y)
    return C2_zone


def depth_2_G(df, depth_vals, X):
    Y = df["G0"]
    G = np.interp(depth_vals, X, Y)
    return G


def get_label(specie, state):
    if state == 0:
        return specie
    elif state == 1:
        return specie + "+"
    else:
        return specie + "+" + str(state)


def add_points(df, X, transitions, npoints):
    """Adding npoints interpolated in Av in between each cut"""
    Av = depth_2_Av(df, transitions, X)
    old_range = np.arange(0, len(Av), 1)
    new_range = np.arange(0, (len(Av) - 1) + (1 / (npoints + 1)), 1 / (npoints + 1))
    new_Av = np.interp(new_range, old_range, Av)
    return Av_2_depth(df, new_Av, X)


def get_transition(df, X, specie, state, molecule=""):
    """return the transition between an element at a given state and the same element in the lower state of excitation.
   if state=0, the transition is the Av where the atomic form is replaced by molecular form. The name of the molecule should then be precised.
   """
    label_up = get_label(specie, state)
    if state != 0:
        label_down = get_label(specie, state - 1)
    else:
        if molecule == "":
            raise Exception(
                "The transition include a molecular state of the element which should be explicity provided !"
            )
        label_down = molecule
    id_transition = np.argwhere(
        np.diff(np.sign(df[label_up] - df[label_down]))
    ).flatten()
    if len(id_transition) == 0:
        depth_transition = np.nan
    else:
        depth_transition = X.iloc[id_transition[0]]
    return depth_transition


def format_species_name(list_of_species):
    element = np.zeros(len(list_of_species), dtype=(np.unicode_, 3))
    state = np.zeros(len(list_of_species), dtype=np.int)
    molecule = np.zeros(len(list_of_species), dtype=(np.unicode_, 6))
    split = [list_of_species[i].split("_") for i in range(len(list_of_species))]
    for i in range(len(split)):
        digits = [int(j) for j in split[i][0] if j.isdigit()]
        if len(digits) != 0:
            state[i] = digits[0]
        element[i] = "".join([j for j in split[i][0] if not j.isdigit() and j != "+"])
        if len(split[i]) >= 2:  # transition between neutral and molecular state
            molecule[i] = list_of_species[i].lstrip("_" + split[i][0])
    return element, state, molecule


def get_depth_transitions(df, X, list_of_species, npoints):
    element, state, molecule = format_species_name(list_of_species)
    transitions = (
        [X.iloc[0]]
        + [
            get_transition(df, X, element[i], state[i], molecule[i])
            for i in range(len(list_of_species))
        ]
        + [X.iloc[-1]]
    )
    new_transitions = add_points(df, X, transitions, npoints)
    return np.array(new_transitions)


def check_if_sorted(array):
    return (np.diff(array) >= 0).all()


def radiative_coef_Pequignot(label, Te, effective=False, case="B"):
    if not effective:
        if ((label == "H1") & (case == "B")) | ((label == "He2") & (case == "B")):
            a, b, c, d = 5.596, -0.6038, 0.3436, 0.4479
            z = 1
        elif (label == "He1") & (case == "A"):
            a, b, c, d = 8.295, -0.5606, 0.9164, 0.2667
            z = 1
    else:
        if (label == "H  1 6562.81A") & (case == "B"):
            a, b, c, d = 2.274, -0.659, 1.939, 0.574
            z = 1
        elif (label == "He 1 4471.49A") & (case == "A"):
            a, b, c, d = 0.429, -0.530, 1.505, 0.779
            z = 1
        elif (label == "He 2 4685.64A") & (case == "B"):
            a, b, c, d = 1.549, -0.693, 2.884, 0.609
            z = 1
    t = 1e-4 * Te / (z ** 2)
    return (1e-13 * z * a * np.power(t, b)) / (np.ones(len(t)) + c * np.power(t, d))


def get_Q_line_Pequignot(dir_, model_name, df, X, line_label, depth_stop=-1, rec=True):
    """
   Compute the number of ionizing photons responsible for producing a given line intensity. 
   Works on radiation bounded or matter bounded models if depth_stop is provided.
   """
    case = "B"  # we use case be for H-like ions
    out = open(dir_ + "/" + model_name + ".out", "r")
    specie = line_label.split()[0]
    state = int(line_label.split()[1])
    if rec:
        atom = pn.RecAtom(specie, state)
    else:
        atom = pn.Atom(specie, state)
    Te = df["Te"]
    wave = get_wavelength_from_label(line_label)
    delta_E = (h * c) / (wave * 1e-10)  # erg
    if line_label == "He 1 4471.49A":
        case = "A"
    emis = radiative_coef_Pequignot(line_label, Te, effective=True, case=case) * delta_E
    alpha_B = radiative_coef_Pequignot(
        specie + str(state), Te, effective=False, case=case
    )
    coefs = alpha_B / emis
    ref_line = np.array(
        [df[line_label.upper()].iloc[0, 0]]
        + [
            df[line_label.upper()].iloc[i, 0] - df[line_label.upper()].iloc[i - 1, 0]
            for i in range(1, len(df))
        ]
    )
    if depth_stop != -1:
        idx_cut = np.argwhere(
            np.diff(np.sign(X - depth_stop * np.ones(len(df))))
        ).flatten()
        if idx_cut.size == 0:
            Q_line = coefs * ref_line
        else:
            Q_line = ref_line[: idx_cut[0]] * coefs[: idx_cut[0]]
    else:
        Q_line = coefs * ref_line
    return np.nansum(Q_line, axis=0)


def get_wavelength_from_label(line_label, rec=True):
    """gets the upper and lower of a given transition defined by its Cloudy label (from .cum)"""
    specie = line_label.split()[0]
    state = int(line_label.split()[1])
    wavelength_lab = float(line_label.split()[2][:-1])
    unit = line_label.split()[2][-1]
    if rec:
        atom = pn.RecAtom(specie, state)
    else:
        atom = pn.Atom(specie, state)
    labels = atom.labels[:100]
    if "_" in labels[0]:
        wavelength = [
            atom.getWave(int(labels[i].split("_")[0]), int(labels[i].split("_")[1]))
            for i in range(len(labels))
        ]
    else:
        wavelength = [float(labels[i]) for i in range(len(labels))]
    if unit == "A":
        idx = np.where(
            [
                int(wavelength[i]) - int(wavelength_lab) == 0
                for i in range(len(wavelength))
            ]
        )[0][0]
    elif unit == "m":
        idx = np.where(
            [
                int(wavelength[i]) - int(wavelength_lab) / 1e4 == 0
                for i in range(len(wavelength))
            ]
        )[0][0]
    return wavelength[idx]


def get_fesc_ratio(df, depth_vals, X):
    Y = df["H  1 6562.81A"].iloc[:, 0]
    L_halpha = np.interp(depth_vals, X, Y)
    h1 = df["HI"]
    h2 = df["HII"]
    idx_IF = np.argwhere(np.diff(np.sign(h2 - h1))).flatten()[0]
    L_halpha_IF = df["H  1 6562.81A"].iloc[idx_IF, 0]
    return np.ones(len(L_halpha)) - np.clip(
        L_halpha / L_halpha_IF, 0, 1
    )  # fesc should always be between 0 and 1


def get_fesc_recomb_lines_Pequignot(
    dir_, name_model, df, depth_vals, X, line_labels=["H  1 6562.81A"]
):
    out = open(dir_ + "/" + name_model + ".out", "r")
    data = [line.split() for line in out if "Q(1.0-1.8):" in line.split()]
    Q_0 = (
        10 ** float(data[0][1])
        + 10 ** float(data[0][3])
        + 10 ** float(data[0][5])
        + 10 ** float(data[0][7])
    )
    Q_line = np.zeros(len(depth_vals))
    for j in range(len(line_labels)):
        Q_line += np.array(
            [
                get_Q_line_Pequignot(
                    dir_,
                    name_model,
                    df,
                    X,
                    line_labels[j],
                    depth_stop=depth_vals[i],
                    rec=True,
                )
                for i in range(len(depth_vals))
            ]
        )
    return np.ones(len(Q_line)) - (Q_line / Q_0)


def get_Q_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emin=1, Emax=np.inf):
    """ Emin and Emax are given in Ryd with 1Ryd=13.605662285137eV"""
    idxs = np.where(
        [
            "#save" in df_ion_cont["#cell(on C scale)"][i]
            for i in range(len(df_ion_cont))
        ]
    )[0]
    Q = []
    # iterate through zones
    for i in range(
        min(len(idxs) - 1, len(X))
    ):  # sometimes not the same number of zones--> we take the min
        nu = np.array(df_ion_cont["nu"][idxs[i] + 1 : idxs[i + 1]])
        f = np.array(df_ion_cont["flux"][idxs[i] + 1 : idxs[i + 1]])
        if (nu.dtype != np.float) | (f.dtype != np.float):
            return np.nan * np.ones(depth_vals)
        integrated_fluxes = np.sum(f[(nu >= Emin) & (nu < Emax)])
        integrated_fluxes *= 4 * np.pi * (X.iloc[i] + R_in) ** 2
        Q += [
            integrated_fluxes,
        ]
    Q = np.array(Q)
    X = X.iloc[: len(Q)]
    Q = np.interp(depth_vals, X, Q)
    return Q


def get_fesc_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emin=1, Emax=np.inf):
    """ Emin and Emax are given in Ryd with 1Ryd=13.605662285137eV"""
    Q = get_Q_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emin=1, Emax=np.inf)
    fesc = Q / np.max(Q)
    return fesc


def get_PP_observable(dir_, model_name, list_of_species, list_of_observables, npoints):
    cond = np.where(summary["Model name"] == model_name)
    if (summary["run"][cond] == "True") & (summary["hdf5"][cond] == "True"):
        h5File = dir_ + "/" + model_name + ".hdf5"
        try:
            df = pd.read_hdf(h5File, key="main", mode="r+")
            df = merge_fix_cols(df)
            df_ion_cont = pd.read_hdf(h5File, key="ion_cont", mode="r+")
            df_ion_cont = merge_fix_cols(df_ion_cont)
        except:
            observables = np.nan * np.ones((ntot_cuts, len(list_of_observables)))
            return np.log10(observables)
        X = df["#depth"]
        depth_vals = get_depth_transitions(df, X, list_of_species, npoints)
        R_in = Rin(dir_, model_name)
        observables = np.zeros((len(depth_vals), len(list_of_observables)))
        for i in range(len(list_of_observables)):
            observables[:, i] = get_PP_1_observable(
                list_of_observables[i],
                dir_,
                model_name,
                df,
                df_ion_cont,
                depth_vals,
                X,
                R_in,
                list_of_species,
                npoints,
            )
    else:
        observables = np.nan * np.ones((ntot_cuts, len(list_of_observables)))
    return np.log10(observables)


def get_PP_1_observable(
    observable,
    dir_,
    name_model,
    df,
    df_ion_cont,
    depth_vals,
    X,
    R_in,
    list_of_species,
    npoints,
):
    """returns the list of values for a given observable in one model (name_model) at given cuts corresponding to the species transitions in list_of_species (+first and last points)"""
    if observable == "depth_stop":
        return depth_vals
    elif observable == "Av_stop":
        return depth_2_Av(df, depth_vals, X)
    elif observable == "Nh_stop":
        return depth_2_colden(df, depth_vals, X)
    elif observable == "G0":
        return depth_2_G(df, depth_vals, X)
    elif observable == "Q0":
        out = open(dir_ + "/" + name_model + ".out", "r")
        data = [line.split() for line in out if "Q(1.0-1.8):" in line.split()]
        if len(data) > 0:
            return (
                10 ** float(data[0][1])
                + 10 ** float(data[0][3])
                + 10 ** float(data[0][5])
                + 10 ** float(data[0][7])
            )
        else:
            return np.nan
    elif observable == "fesc(Ha)":
        return get_fesc_ratio(df, depth_vals, X)
    elif observable == "fesc(QHa)":
        return get_fesc_recomb_lines_Pequignot(dir_, name_model, depth_vals, X)
    elif observable == "MH2_total":
        return depth_2_MH2(df, depth_vals, X, R_in, zone="total")
    elif observable == "MH2_CO":
        return depth_2_MH2(df, depth_vals, X, R_in, zone="CO")
    elif observable == "MH2_C":
        return depth_2_MH2(df, depth_vals, X, R_in, zone="C")
    elif observable == "MH2_C+":
        return depth_2_MH2(df, depth_vals, X, R_in, zone="C+")
    elif observable == "MHI":
        return depth_2_MHI(df, depth_vals, X, R_in)
    elif observable == "MHII":
        return depth_2_MHII(df, depth_vals, X, R_in)
    elif observable == "Mdust":
        return Mdust(dir_, name_model, df, depth_vals, X, R_in)
    elif observable == "fesc(1-infty)":
        return get_fesc_from_cont(df_ion_cont, df, depth_vals, X, R_in)
    elif observable == "fesc(1-1.8)":
        return get_fesc_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emax=1.8)
    elif observable == "fesc(1.8-4)":
        return get_fesc_from_cont(
            df_ion_cont, df, depth_vals, X, R_in, Emin=1.8, Emax=4
        )
    elif observable == "fesc(4-20)":
        return get_fesc_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emin=4, Emax=20)
    elif observable == "fesc(20-infty)":
        return get_fesc_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emin=20)
    elif observable == "Q(1-infty)":
        return get_Q_from_cont(df_ion_cont, df, depth_vals, X, R_in)
    elif observable == "Q(1-1.8)":
        return get_Q_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emax=1.8)
    elif observable == "Q(1.8-4)":
        return get_Q_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emin=1.8, Emax=4)
    elif observable == "Q(4-20)":
        return get_Q_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emin=4, Emax=20)
    elif observable == "Q(20-infty)":
        return get_Q_from_cont(df_ion_cont, df, depth_vals, X, R_in, Emin=20)

    elif observable == "CII_H+":
        return depth_2_CII(df, depth_vals, X, zone="H+")
    elif observable == "CII_H":
        return depth_2_CII(df, depth_vals, X, zone="H")
    elif observable == "CII_H2":
        return depth_2_CII(df, depth_vals, X, zone="H2")
    else:
        raise Exception(
            "The observable name " + observable + "has not been coded yet !!"
        )


#######################################################################


def parse_model_name(model_name):
    """
   Parse model name string to get parameter values: all values are return as log10(value). Age in in Myr and Z in Zsun units.
   """
    tmp = model_name.split("_")
    return {
        "lum": int(tmp[0][3:]),
        "age": np.log10(float(tmp[1][3:-3])),
        "n": float(tmp[2][1:]),
        "u": float(tmp[3][1:]),
        "Z": np.log10(float(tmp[4][1:])),
        "Lx": np.log10(float(tmp[5][2:]) + (1 - np.sign(float(tmp[5][2:]))) * eps),
        "Tx": float(tmp[6][2:]),
        "cut": float(tmp[7][3:]),
        "q_PAH": 0,  #'median'
        "Z_dust": 0,
    }  #'median'


############################################################################
