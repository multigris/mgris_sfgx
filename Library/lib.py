import numpy as np

import matplotlib

# matplotlib.use('TkAgg')

from matplotlib.patches import Arc, Wedge
from matplotlib import rc
import matplotlib.pyplot as plt
import random

# global parameters
from rcparams import rcParams

import logging

from scipy.interpolate import interp1d
from math import pi

from scipy.stats import norm

from Contexts.Library.extinction_rule import (
    extinction_rule,
    extinction_rule_persector,
    parse_cloudy_label,
)


# -------------------------------------------------------------------
def context_specific_plots(trace, inp, verbose=False):
    # full spectrum
    # logging.info('- spectrum plots')
    # spectrumplot(trace, inp)

    logging.info("- line mosaic plot")
    linemosaic(trace, inp)

    logging.info("- sector plots")
    try:
        if inp["plaw_params"] != []:
            sectorplot_distrib(trace, inp, verbose=verbose)
        else:
            sectorplot(trace, inp, verbose=verbose)
            sectorplot(trace, inp, explode=False, verbose=verbose)
    except:
        logging.error("/!\ Sector plot not working")

    return


# -------------------------------------------------------------------
def context_specific_rules(inp, verbose=False):
    # any custom rules for inference here

    rules = {}

    res, log = extinction_rule(inp, verbose=verbose)
    rules.update(res)
    res, log = extinction_rule_persector(inp, verbose=verbose)
    rules.update(res)

    logging.info(log)

    # ==========================================
    # OTHER RULES

    # params_eval = "pm.HalfNormal('ebv', sigma=10)" #possibility to have several commands with ;
    # scale_context = "{o: cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=model['ebv']) for i,o in enumerate(inp['observations'].names)}" #possibility to have several commands with ;
    # scale_context_add2trace = "{o: cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=trace.posterior['ebv']) for i,o in enumerate(inp['observations'].names)}" #possibility to have several commands with ;
    # we'll take the log with tt afterwards
    # rules.update({'extinction': {'params': ['ebv'],
    #                              'params_eval': params_eval, #pm parameters
    #                              'scale_context': scale_context, #for mgris_search
    #                              'scale_context_add2trace': scale_context_add2trace, #for add2trace in mgris_process and mgris_post-process
    #                              'cs_args': cs_args} #args to pass for the evals
    # })

    return rules


# -------------------------------------------------------------------
def context_specific_inference_scaling_bu(inp, verbose=False):
    # any custom rules for inference here
    activate = True

    if activate:
        # maybe use dust *attenuation* curves? see https://dust-attenuation.readthedocs.io/en/latest/#reference-api
        grain_model = WD01("LMCAvg")

        # read all wavelengths
        inp["observations"].wavelength_inv = np.array(inp["observations"].values) * 0.0

        # first, get 1/wave[um]
        for i, name in enumerate(inp["observations"].names):
            specie, wavelength, unit = parse_cloudy_label(
                name
            )  # wavelength will be in um
            # validity domain for WD01
            wavelength = np.max([grain_model.x_range[0], wavelength])
            wavelength = np.min([grain_model.x_range[1], wavelength])
            inp["observations"].wavelength_inv[i] = 1 / wavelength

        cs_args = [
            grain_model,
        ]

        # ext.extinguish seems to work with tensors #x unit = um-1
        params_eval = "pm.HalfNormal('ebv', sigma=10)"  # possibility to have several commands with ;
        scale_context = "{o: cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=model['ebv']) for i,o in enumerate(inp['observations'].names)}"  # possibility to have several commands with ;
        scale_context_add2trace = "{o: cs_args[0].extinguish(inp['observations'].wavelength_inv[i], Ebv=trace.posterior['ebv']) for i,o in enumerate(inp['observations'].names)}"  # possibility to have several commands with ;
        # we'll take the log with tt afterwards

        return {
            "params_eval": params_eval,
            "scale_context": scale_context,
            "scale_context_add2trace": scale_context_add2trace,
            "cs_args": cs_args,
        }

    else:
        return None


# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# CUSTOM FUNCTIONS BELOW
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# U to radius (same L for source for all sectors)
def torad(angle):
    return angle * pi / 180.0


def plot_lineangle(ax, origin, angle, length, *args, **kwargs):
    x = origin[0] + length * np.cos(torad(angle))
    y = origin[1] + length * np.sin(torad(angle))
    ax.plot([origin[0], x], [origin[1], y], *args, **kwargs)
    return x, y


# -------------------------------------------------------------------
# plot sectors
global RMIN
RMIN = 2


# U to radius (same L for source for all sectors)
def U2r(U):
    return -(U - 1 - RMIN)


# cut to n
def cut2n(cut):
    return interp1d([0, 1, 2, 3, 4], [0, 0, 1.5, 2, 3])(cut)


def cut2rout(rin, cut):
    scale = 1.3  # to expand sectors
    return scale * (rin + cut)


def sectorplot(trace, inp, explode=True, verbose=False):  # g->group
    """
    Density: color
    U: radius
    """

    n_comps = inp["n_comps"]
    groups = inp["groups"]
    n_groups = len(groups)

    plt.clf()

    # matplotlib.rcParams['text.usetex']=True
    # if 'text.latex.unicode' in matplotlib.rcParams.keys():
    #     matplotlib.rcParams['text.latex.unicode'] = True
    # elif 'text.latex.preview' in matplotlib.rcParams.keys():
    #         matplotlib.rcParams['text.latex.preview'] = True

    # plt.rc('text', usetex=False)
    # plt.rc('font', family='serif')

    scales = [
        np.quantile(np.sum([trace["w"][:, :, s] for s in groups[g]], axis=(0)), 0.5)
        for g in range(n_groups)
    ]
    wratio = scales - np.min(scales) + 1
    wratio /= np.max(wratio)
    wratio = [np.max([w, 0.2]) for w in wratio]

    fig, axs = plt.subplots(
        1, n_groups, figsize=(5 * n_groups, 6), gridspec_kw={"width_ratios": wratio}
    )
    if n_groups == 1:
        axs = [
            axs,
        ]

    # color map for density
    cmap = matplotlib.cm.get_cmap("YlOrRd")
    normalize = matplotlib.colors.Normalize(vmin=0, vmax=5)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=normalize)

    z_text, z_stars, z_center_spot, z_ugrid, z_wedge_in, z_wedge_out, z_arc, z_line = (
        300,
        250,
        240,
        240,
        230,
        22,
        250,
        250,
    )
    # depth = 4
    cols = rcParams["sector_colors"]

    starspread, starsize = 2, 4

    for g in range(n_groups):
        # age is supposed to be the same for all input sectors
        age = 4 * 10 ** (np.quantile(trace["age_{}".format(groups[g][0])], 0.5))

        if "lum_0" in trace.keys():
            nstars = round(
                10 ** (float(trace["lum_{}".format(groups[g][0])].median().data) - 7)
            )
        else:
            nstars = round(10 * scales[g] * 10 ** (float(trace["scale"].median().data)))

        sectors = [
            {
                "w": float(
                    (
                        trace["w_{}".format(s)]
                        / np.sum(
                            [trace["w_{}".format(ss)] for ss in groups[g]], axis=(0)
                        )
                    )
                    .mean()
                    .data
                ),
                "u": float(trace["u_{}".format(s)].median().data)
                if f"u_{s}" in trace.keys()
                else -2,
                "n": float(trace["n_{}".format(s)].median().data)
                if f"n_{s}" in trace.keys()
                else 1,
            }
            for s in groups[g]
        ]
        for j, s in enumerate(groups[g]):
            if "cut_0" in trace.keys():
                sectors[j].update(
                    {"cut": float(trace["cut_{}".format(s)].median().data)}
                )
            else:
                sectors[j].update({"cut": 3})

        # special case for DIG sector beyond matter-bounded sector
        if "configuration" in inp.keys():
            if "DIG" in inp["configuration"]:
                # normalize first
                maxw = np.max([s["w"] for s in sectors])
                for i, s in enumerate(sectors):
                    sectors[s]["w"] /= maxw

        axs[g].set_aspect(
            "equal"
        )  # possible to use only if we don't use gridspec option in subplots
        axs[g].set_axis_off()
        axs[g].set_xlim(-15, 15)
        axs[g].set_ylim(-15, 15)

        if n_groups > 1:
            axs[g].set_title("Group #{}".format(g), y=1.0, pad=-14 * wratio[g])

        # draw angles, arcs, wedges
        theta1 = 0  # *u.deg
        # angle_scale = 8 #2, 4, 8, 10
        angle_scale = 2
        if angle_scale > 360 * np.min([s["w"] for s in sectors]):
            angle_scale = int(np.ceil(360 * np.min([s["w"] for s in sectors])))
        # angle_scale = int(np.ceil(360*np.min([s['w'] for s in sectors])))
        max_angle = int(360 / angle_scale)

        remaining_angles = range(0, max_angle)  # initial list of all possible angle

        for i, s in enumerate(sectors):
            # reinitialize if DIG sector beyond matter-bounded sector
            if "configuration" in inp.keys():
                if "DIG" in inp["configuration"]:
                    remaining_angles = range(0, max_angle)

            rin = U2r(s["u"])
            rout = cut2rout(rin, s["cut"])  # depth

            # print(remaining_angles, s['w'], max_angle, angle_scale)

            if explode:
                angles = random.sample(
                    sorted(set(remaining_angles)),
                    np.min([int(round(max_angle * s["w"])), len(remaining_angles)]),
                )
                for a in angles:
                    wedge_in = axs[g].add_artist(
                        Wedge(
                            (0.0, 0.0),
                            rin,
                            a * angle_scale,
                            (a + 1) * angle_scale,
                            facecolor="white",
                            color="white",
                            alpha=1,
                            fill=True,
                            zorder=z_wedge_in,
                        )
                    )
                    wedge_in.set_antialiased(True)
                    # wedge_out = axs[g].add_artist(Wedge((0.,0.), rout, a*angle_scale, (a+1)*angle_scale, facecolor=cmap(normalize(s['n'])), hatch=None, alpha=0.75, fill = True, zorder=z_wedge_out))
                    # wedge_out.set_antialiased(True)
                    for ii, cut in enumerate(np.arange(s["cut"], 0, -0.1)):
                        n = s["n"] * (1 + cut2n(cut))
                        wedge_out = axs[g].add_artist(
                            Wedge(
                                (0.0, 0.0),
                                cut2rout(rin, cut),
                                a * angle_scale,
                                (a + 1) * angle_scale,
                                facecolor=cmap(normalize(n)),
                                hatch=None,
                                alpha=0.75,
                                fill=True,
                                zorder=z_wedge_out + ii,
                            )
                        )
                        wedge_out.set_antialiased(True)

                    # arc_in = axs[g].add_patch(Arc((0, 0), 2*rin, 2*rin, theta1=a*angle_scale, theta2=(a+1)*angle_scale, edgecolor=cols[i], lw=2, zorder=z_arc))
                    # arc_out = axs[g].add_patch(Arc((0, 0), 2*rout, 2*rout, theta1=a*angle_scale, theta2=(a+1)*angle_scale, edgecolor=cols[i], lw=2, zorder=z_arc))

                remaining_angles = np.setdiff1d(remaining_angles, angles)

            else:
                theta2 = theta1 + 360 * s["w"]  # *u.deg

                plot_lineangle(
                    axs[g],
                    [0.0, 0.0],
                    theta1,
                    rout,
                    color="black",
                    linestyle="-",
                    zorder=z_line,
                )
                plot_lineangle(
                    axs[g],
                    [0.0, 0.0],
                    theta2,
                    rout,
                    color="black",
                    linestyle="-",
                    zorder=z_line,
                )

                wedge_in = axs[g].add_artist(
                    Wedge(
                        (0.0, 0.0),
                        rin,
                        theta1,
                        theta2,
                        color="white",
                        facecolor="white",
                        alpha=1,
                        fill=True,
                        zorder=z_wedge_in,
                    )
                )
                # wedge_out = axs[g].add_artist(Wedge((0.,0.), rout, theta1, theta2, facecolor=cmap(normalize(s['n'])), hatch=None, alpha=1, fill = True, zorder=z_wedge_out))
                for ii, cut in enumerate(np.arange(s["cut"], 0, -0.1)):
                    n = s["n"] * (1 + cut2n(cut))
                    wedge_out = axs[g].add_artist(
                        Wedge(
                            (0.0, 0.0),
                            cut2rout(rin, cut),
                            theta1,
                            theta2,
                            facecolor=cmap(normalize(n)),
                            hatch=None,
                            alpha=1,
                            fill=True,
                            zorder=z_wedge_out + ii,
                        )
                    )
                    wedge_out.set_antialiased(True)

                # annotation
                angle = 0.5 * (theta1 + theta2)
                x = rout * np.cos(torad(angle))
                y = rout * np.sin(torad(angle))
                t = axs[g].annotate(
                    "#{}".format(i + 1),
                    xy=(x, y),
                    xytext=(x, y),
                    zorder=z_text,
                    fontweight="bold",
                    color=cols[i],
                    fontsize=10,
                    horizontalalignment={-1: "right", 1: "left"}[int(np.sign(x))],
                    verticalalignment={-1: "top", 1: "bottom"}[int(np.sign(y))],
                )
                t.set_bbox(dict(facecolor="white", alpha=0.7, edgecolor="white"))

                # U grid
                for uvalue in np.arange(-4, 0):
                    if U2r(uvalue) < rin:
                        axs[g].add_patch(
                            Arc(
                                (0, 0),
                                2 * U2r(uvalue),
                                2 * U2r(uvalue),
                                theta1=theta1,
                                theta2=theta2,
                                edgecolor="gray",
                                linestyle=":",
                                lw=2,
                                zorder=z_ugrid,
                            )
                        )

                # arcs
                arc_in = axs[g].add_patch(
                    Arc(
                        (0, 0),
                        2 * rin,
                        2 * rin,
                        theta1=theta1,
                        theta2=theta2,
                        edgecolor=cols[i],
                        lw=2,
                        zorder=z_arc,
                    )
                )
                arc_out = axs[g].add_patch(
                    Arc(
                        (0, 0),
                        2 * rout,
                        2 * rout,
                        theta1=theta1,
                        theta2=theta2,
                        edgecolor=cols[i],
                        lw=2,
                        zorder=z_arc,
                    )
                )

                # ionization front
                r = cut2rout(rin, 1)
                if r < rout:
                    arc_IF = axs[g].add_patch(
                        Arc(
                            (0, 0),
                            2 * r,
                            2 * r,
                            theta1=theta1,
                            theta2=theta2,
                            edgecolor="gray",
                            lw=2,
                            zorder=z_arc,
                        )
                    )

                # dissociation front
                r = cut2rout(rin, 2)
                if r < rout:
                    arc_DF = axs[g].add_patch(
                        Arc(
                            (0, 0),
                            2 * r,
                            2 * r,
                            theta1=theta1,
                            theta2=theta2,
                            edgecolor="gray",
                            lw=2,
                            zorder=z_arc,
                        )
                    )

                theta1 = theta2

        # center spot
        center = axs[g].add_artist(
            Wedge((0.0, 0.0), RMIN, 0.0, 360.0, color="yellow", alpha=1)
        )
        center.zorder = z_center_spot

        # star cluster
        for i in range(nstars):
            x, y = 0, 0
            ds = starsize * np.random.uniform(low=1, high=15 / age)
            dx, dy = (
                starspread * np.array(np.random.random() - 0.5),
                starspread * np.array(np.random.random() - 0.5),
            )
            axs[g].plot(
                x + dx,
                y + dy,
                "*",
                color="white",
                markersize=4 * ds,
                fillstyle="full",
                markeredgewidth=0.0,
                markeredgecolor="cyan",
                alpha=np.min([1, 2 / nstars]),
                zorder=z_stars,
            )  # markeredgecolor='gray'
            axs[g].plot(
                x + dx,
                y + dy,
                "*",
                color="cyan",
                markersize=3 * ds,
                fillstyle="full",
                markeredgewidth=0.0,
                markeredgecolor="cyan",
                alpha=np.min([1, 2 / nstars]),
                zorder=z_stars,
            )
            axs[g].plot(
                x + dx,
                y + dy,
                "*",
                color="blue",
                markersize=2 * ds,
                fillstyle="full",
                markeredgewidth=0.0,
                markeredgecolor="blue",
                alpha=np.min([1, 5 / nstars]),
                zorder=z_stars,
            )

        # ------------------------------
        # fake up the array of the scalar mappable. Urgh…
        # sm._A = []
        # cb = plt.colorbar(sm, orientation='horizontal', pad = 0.05, shrink=0.7)
        # cb.set_label('Log density [cm$^{-3}$]', labelpad=-50, y=1.05, rotation=0, fontsize=10)

    # ------------------------------
    # fig.suptitle('Preliminary', fontsize=10, fontweight='bold')
    fig.subplots_adjust(
        left=0.0, bottom=0.0, right=1.0, top=1.0, wspace=0.0, hspace=0.0
    )
    # plt.show()

    exploded_str = "_exploded" if explode else ""

    fig.savefig(
        "{}/sectors{}.png".format(inp["output_directory"] + "/Plots/", exploded_str)
    )
    plt.close(fig)


# ------------------------------
# ------------------------------
# ------------------------------
# ------------------------------
def sectorplot_distrib(trace, inp, verbose=False):  # g->group
    """
    Density: color
    U: radius
    """

    plt.clf()

    fig, ax = plt.subplots(1, 1, figsize=(5, 6))

    # color map for density
    cmap = matplotlib.cm.get_cmap("YlOrRd")
    normalize = matplotlib.colors.Normalize(vmin=0, vmax=5)
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=normalize)

    z_text, z_stars, z_center_spot, z_ugrid, z_wedge_in, z_wedge_out, z_arc, z_line = (
        300,
        250,
        240,
        240,
        230,
        22,
        250,
        250,
    )
    # depth = 4
    cols = rcParams["sector_colors"]

    starspread, starsize = 2, 4

    # age is supposed to be the same for all input sectors
    age = 4 * 10 ** (np.quantile(trace["age_0"], 0.5))

    if "lum_0" in trace.keys():
        nstars = round(10 ** (float(trace["lum_0"].median().data) - 7))
    else:
        nstars = round(10 * 10 ** (float(trace["scale"].median().data)))

    minw = 1
    for p in ("u", "n", "cut"):
        if p in inp["plaw_params"]:
            vals = np.array(
                [
                    tmp
                    for tmp in inp["grid"].params.values[p]
                    if tmp >= trace[f"lowerbound_{p}"].median().round()
                    and tmp <= trace[f"upperbound_{p}"].median().round()
                ]
            )
            w = vals * (
                1 + float(trace[f"alpha_{p}"].median())
            )  # that's what we add in log
            w = 10**w
            w /= np.sum(w)
            minw = np.min([minw, np.min(w)])
            # print(w, minw)

    # draw angles, arcs, wedges
    angle_scale = int(np.ceil(minw * 360))
    angle_scale = np.max([1, angle_scale])
    max_angle = int(np.ceil(360 / angle_scale))

    parts = {}
    for p in ("u", "n", "cut"):
        parts.update({p: np.zeros(max_angle)})
        if p in inp["plaw_params"]:
            vals = np.array(
                [
                    tmp
                    for tmp in inp["grid"].params.values[p]
                    if tmp >= trace[f"lowerbound_{p}"].median().round()
                    and tmp <= trace[f"upperbound_{p}"].median().round()
                ]
            )
            w = vals * (1 + float(trace[f"alpha_{p}"].median()))
            w /= np.sum(w)
            # print(p, vals, w)
            remaining_angles = range(0, max_angle)  # initial list of all possible angle
            for i, val in enumerate(vals):
                angles = random.sample(
                    sorted(set(remaining_angles)),
                    np.min([int(round(max_angle * w[i])), len(remaining_angles)]),
                )
                parts[p][angles] = val
                remaining_angles = np.setdiff1d(remaining_angles, angles)
        elif p in inp["brokenplaw_params"]:
            vals1 = np.array(
                [
                    tmp
                    for tmp in inp["grid"].params.values[p]
                    if tmp >= trace[f"lowerbound_{p}"].median().round()
                    and tmp < trace[f"pivot_{p}"].median().round()
                ]
            )
            vals2 = np.array(
                [
                    tmp
                    for tmp in inp["grid"].params.values[p]
                    if tmp >= trace[f"pivot_{p}"].median().round()
                    and tmp <= trace[f"upperbound_{p}"].median().round()
                ]
            )
            w = np.hstack(
                [
                    vals1 * (1 + float(trace[f"alpha1_{p}"].median())),
                    vals2 * (1 + float(trace[f"alpha2_{p}"].median())),
                ]
            )
            w /= np.sum(w)
            # print(p, vals, w)
            remaining_angles = range(0, max_angle)  # initial list of all possible angle
            for i, val in enumerate(vals):
                angles = random.sample(
                    sorted(set(remaining_angles)),
                    np.min([int(round(max_angle * w[i])), len(remaining_angles)]),
                )
                parts[p][angles] = val
                remaining_angles = np.setdiff1d(remaining_angles, angles)
        else:
            # assuming only one sector
            parts[p][angles] = float(trace[f"{p}_0"].median())

    ax.set_aspect(
        "equal"
    )  # possible to use only if we don't use gridspec option in subplots
    ax.set_axis_off()
    ax.set_xlim(-15, 15)
    ax.set_ylim(-15, 15)

    for a in np.arange(max_angle):
        u = parts["u"][a]
        n = parts["n"][a]
        cut = parts["cut"][a]

        rin = U2r(u)
        rout = cut2rout(rin, cut)  # depth

        wedge_in = ax.add_artist(
            Wedge(
                (0.0, 0.0),
                rin,
                a * angle_scale,
                (a + 1) * angle_scale,
                facecolor="white",
                color="white",
                alpha=1,
                fill=True,
                zorder=z_wedge_in,
            )
        )
        wedge_in.set_antialiased(True)

        for ii, c in enumerate(np.arange(cut, 0, -0.1)):
            n = n * (1 + cut2n(c))
            wedge_out = ax.add_artist(
                Wedge(
                    (0.0, 0.0),
                    cut2rout(rin, c),
                    a * angle_scale,
                    (a + 1) * angle_scale,
                    facecolor=cmap(normalize(n)),
                    hatch=None,
                    alpha=0.75,
                    fill=True,
                    zorder=z_wedge_out + ii,
                )
            )
            wedge_out.set_antialiased(True)

    # center spot
    center = ax.add_artist(Wedge((0.0, 0.0), RMIN, 0.0, 360.0, color="yellow", alpha=1))
    center.zorder = z_center_spot

    # U grid
    for uvalue in np.arange(-4, 0):
        ax.add_patch(
            Arc(
                (0, 0),
                2 * U2r(uvalue),
                2 * U2r(uvalue),
                theta1=0,
                theta2=360,
                edgecolor="gray",
                linestyle=":",
                lw=2,
                zorder=z_ugrid,
            )
        )

    # star cluster
    for i in range(nstars):
        x, y = 0, 0
        ds = starsize * np.random.uniform(low=1, high=15 / age)
        dx, dy = (
            starspread * np.array(np.random.random() - 0.5),
            starspread * np.array(np.random.random() - 0.5),
        )
        ax.plot(
            x + dx,
            y + dy,
            "*",
            color="white",
            markersize=4 * ds,
            fillstyle="full",
            markeredgewidth=0.0,
            markeredgecolor="cyan",
            alpha=np.min([1, 2 / nstars]),
            zorder=z_stars,
        )  # markeredgecolor='gray'
        ax.plot(
            x + dx,
            y + dy,
            "*",
            color="cyan",
            markersize=3 * ds,
            fillstyle="full",
            markeredgewidth=0.0,
            markeredgecolor="cyan",
            alpha=np.min([1, 2 / nstars]),
            zorder=z_stars,
        )
        ax.plot(
            x + dx,
            y + dy,
            "*",
            color="blue",
            markersize=2 * ds,
            fillstyle="full",
            markeredgewidth=0.0,
            markeredgecolor="blue",
            alpha=np.min([1, 5 / nstars]),
            zorder=z_stars,
        )

    # ------------------------------
    fig.subplots_adjust(
        left=0.0, bottom=0.0, right=1.0, top=1.0, wspace=0.0, hspace=0.0
    )
    # plt.show()

    fig.savefig("{}/sectors_exploded.png".format(inp["output_directory"] + "/Plots/"))
    plt.close(fig)


# -------------------------------------------------------------------
def planck(wav, T):
    h = 6.626e-34
    c = 3.0e8
    k = 1.38e-23
    a = 2.0 * h * c**2
    b = h * c / (wav * k * T)
    intensity = a / ((wav**5) * (np.exp(b) - 1.0))
    return intensity


# -------------------------------------------------------------------
def spectrumplot(trace, inp):
    inp["observations"].names

    lines = {}

    for name in inp["observations"].names:
        specie, wavelength, unit = parse_cloudy_label(name)

        lines.update(
            {
                name: {
                    "specie": specie,
                    "wavelength": np.log10(float(wavelength)),
                    "modflux": float(trace[name].mean()),
                    "obsflux": inp["observations"].values[
                        np.where(np.array(inp["observations"].names) == name)[0][0]
                    ],
                }
            }
        )

    # ===============================================

    fig, axs = plt.subplots(3, 1, figsize=(5, 6), sharex=True)

    sdev = 0.005
    x = np.arange(
        np.min([lines[line]["wavelength"] for line in lines]),
        np.max([lines[line]["wavelength"] for line in lines]),
        0.001,
    )

    continuum = np.sum(
        [planck(10**x * 1e-6, T) for T in np.arange(100, 10, -10)], axis=0
    )

    totalobs = np.sum(
        [
            10 ** (lines[line]["obsflux"] - inp["order_of_magnitude_obs"])
            * norm.pdf(x, lines[line]["wavelength"], sdev)
            for line in lines
        ],
        axis=0,
    )

    totalmod = np.sum(
        [
            10 ** (lines[line]["modflux"] - inp["order_of_magnitude_obs"])
            * norm.pdf(x, lines[line]["wavelength"], sdev)
            for line in lines
        ],
        axis=0,
    )

    for ax in axs:
        ax.set_xlim([np.min(x), np.max(x)])

    axs[2].set_xlabel("Log wavelength [um]")

    axs[0].plot(x, totalmod + continuum, color="red", alpha=0.5)
    axs[0].plot(x, totalobs + continuum, color="blue", alpha=0.5)

    axs[1].plot(x, totalmod, color="red", alpha=0.5)
    axs[1].plot(x, totalobs, color="blue", alpha=0.5)

    axs[2].plot(x, totalmod - totalobs, color="black")
    axs[2].axhline(0, color="black")

    axlinear = axs[0].twiny()
    axlinear.set_xlim([10 ** axs[0].get_xlim()[0], 10 ** axs[0].get_xlim()[1]])
    axlinear.set_xscale("log")
    axlinear.set_xlabel("Wavelength [um]")

    for line in lines:
        modflux = 10 ** (
            lines[line]["obsflux"] - inp["order_of_magnitude_obs"]
        ) * norm.pdf(x, lines[line]["wavelength"], sdev)

        obsflux = 10 ** (
            lines[line]["modflux"] - inp["order_of_magnitude_obs"]
        ) * norm.pdf(x, lines[line]["wavelength"], sdev)

        axs[0].plot(x, modflux + continuum, color="red", alpha=0.5)
        axs[0].plot(x, obsflux + continuum, color="blue", alpha=0.5)

        axs[1].plot(x, modflux, color="red", alpha=0.5)
        axs[1].plot(x, obsflux, color="blue", alpha=0.5)

        for ax in axs:
            ax.axvline(lines[line]["wavelength"], color="black", alpha=0.2)

        axs[1].text(
            lines[line]["wavelength"],
            np.max(modflux),
            line,
            rotation="vertical",
            alpha=0.2,
            fontsize="small",
        )

    axs[0].set_yscale("log")

    fig.subplots_adjust(wspace=0, hspace=0)

    fig.savefig("{}/spectra.png".format(inp["output_directory"] + "/Plots/"))
    plt.close(fig)

    return


# --------------------------------------------------------------------
def sig2fwhm(sig):
    return sig * 2.0 * np.sqrt(2.0 * np.log(2))


def Gauss1D(x, pctr, pmax=None, parea=None, psig=None, pfwhm=None):
    if pmax is None and parea is None:
        print("Provide any of pmax or parea...")
        return None
    if pmax is not None and parea is not None:
        print("Cannot use max and area at the same time...")
        return None
    if psig is not None and pfwhm is not None:
        print("Cannot use sigma and fwhm at the same time...")
        return None
    if psig is None and pfwhm is None:
        print("Use either psig or pfwhm...")
        return None

    if pfwhm is not None:
        psig = sig2fwhm(pfwhm)
    if parea is not None:
        pmax = parea / (psig * np.sqrt(2.0 * pi))

    return pmax * np.exp(-((x - pctr) ** 2) / (2 * psig**2))


# -------------------------------------------------------------------
def specie2t(s):
    if s in (
        "He1",
        "He2",
        "C3",
        "C4",
        "N3",
        "N4",
        "O3",
        "O4",
        "Ne2",
        "Ne3",
        "Si3",
        "P3",
        "S3",
        "S4",
        "Cl3",
        "Ar3",
        "Ar4",
        "Fe3",
        "Fe4",
        "Fe5",
    ):
        return 1e4
    elif s in ("N2", "Ar2", "S2"):
        return 5e3
    elif s in ("H1", "C2", "N1", "O1", "Si2", "P2", "Cl2", "Fe2", "H2"):
        return 5e2
    elif s in ("CO"):
        return 5e2
    else:
        return 1e2  # default...


# approximate mass
def Watom(specie):
    u = 1.6605e-27  # kg
    if specie == "H2":
        return 2 * u
    elif specie == "CO":
        return 28 * u
    else:
        atom = "".join([i for i in specie if not i.isdigit()])  # strip number
        # https://www.lenntech.com/periodic/mass/atomic-mass.htm
        mass = {
            "H": 1.0079,
            "He": 4.0026,
            "C": 12.0107,
            "N": 14.0067,
            "O": 15.9994,
            "Ne": 20.1797,
            "Si": 28.0855,
            "P": 30.9738,
            "S": 32.065,
            "Cl": 35.453,
            "Ar": 39.948,
            "Fe": 55.845,
        }
        if atom in mass.keys():
            return mass[atom] * u
        else:
            return 100 * u  # default...


def linemosaic(trace, inp, verbose=False):
    arrok = []
    for i, o in enumerate(inp["observations"].names):
        try:
            specie, wavelength, unit = parse_cloudy_label(o)
            if not inp["observations"].obs[o].lower:
                arrok += [
                    i,
                ]
        except:
            logging.warning("Observable {} not recognized as a line".format(o))

    nlines = len(arrok)
    ncols = rcParams["plot_ncols"]
    nrows = int(np.ceil(nlines / ncols))

    fig, axs = plt.subplots(
        nrows=nrows,
        ncols=ncols,
        sharey="row",
        sharex="col",
        figsize=(2 * ncols, 2 * nrows),
    )
    if nrows == 1:
        axs = np.array(axs, ndmin=2)

    # remove frames
    for i in range(ncols):
        for j in range(nrows):
            axs[j, i].set_axis_off()

    vturb = 2e3  # m s-1
    k = 1.3807e-23  # m2 kg s-2 K-1
    c = 3e8  # m s-1

    # main loop
    l = 0
    psigmax = 0
    nsigrange = 3.5
    for i in range(ncols):
        for j in range(nrows):
            if l >= nlines:
                continue
            o = inp["observations"].names[arrok[l]]
            specie, wavelength, unit = parse_cloudy_label(o)  # wavelength will be in um
            m = Watom(specie)
            # delta in km s-1
            psig = 1e-3 * np.sqrt(2 * k * specie2t(specie) / m + vturb**2)
            # print(o, psig, float(wavelength)*psig/3e5)
            psigmax = np.max([psig, psigmax])

            ax = axs[j, i]
            ax.set_axis_on()

            npersig = 3
            npts = int(np.ceil(npersig * 2 * 5 * nsigrange))
            x = np.linspace(-5 * nsigrange * psig, 5 * nsigrange * psig, npts)
            if inp["observations"].obs[o].upper:
                nsig = 1
            else:
                nsig = (
                    np.mean(inp["observations"].delta[arrok[l]])
                    / inp["observations"].values[arrok[l]]
                )
            nsig *= 1  # npersig
            noise = np.random.normal(0, nsig, npts)
            ax.step(x, Gauss1D(x, 0, psig=psig, pmax=1) + noise, where="mid", label=o)

            tx = trace[o].data.flatten()
            # take only 100 samples
            rnd = random.sample(range(len(tx)), 100)
            tx = tx[rnd]
            for ii in range(len(tx)):
                ax.plot(
                    x,
                    Gauss1D(
                        x,
                        0,
                        psig=psig,
                        pmax=tx[ii] / inp["observations"].values[arrok[l]],
                    ),
                    alpha=5 / len(tx),
                    color="red",
                )

            ax.axvline(0, color="black", alpha=0.2)
            ax.axhline(0, color="black", alpha=0.2)

            ax.legend(fontsize=8)

            if j == nrows - 1:
                ax.set_xlabel("$\Delta{v}$ [km s$^{-1}$]")
            if i == 0:
                ax.set_ylabel("Norm. flux density")

            l += 1

    for i in range(ncols):
        for j in range(nrows):
            axs[j, i].set_xlim([-nsigrange * psigmax, nsigrange * psigmax])

    # fig.suptitle('Simulated line spectra')
    fig.subplots_adjust(wspace=0, hspace=0)

    fig.tight_layout()
    fig.savefig("{}/line_mosaic.png".format(inp["output_directory"] + "/Plots/"))

    return
