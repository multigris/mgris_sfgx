# This is when running the script from the root mgris directory
import sys, os

s = os.path.dirname(os.path.abspath(__file__))
if "Contexts" in s:  # add root
    s = s[: s.index("Contexts/")]
    sys.path.insert(0, s)

import argparse
import numpy as np

import pandas as pd
from Library.lib_mc import logsum_simple
from Library.lib_main import readparam, read_data, check_duplicates
from Library.lib_misc import FileExists, inputRL

# import dask.dataframe as dd

from copy import deepcopy
from tqdm import tqdm

from rcparams import rcParams

import numpy

try:
    import jax.numpy as np
    

    # making sure that device is visible
    jax.default_backend()
    jax.devices()
except:
    import numpy as np


# =================================================================
def main(args=None):
    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"
    grid_directory = root_directory + "/Grids/"

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS pre-processing step")
        parser.add_argument(
            "--chunks", "-c", help="Read as chunks", action="store_true"
        )
    args = parser.parse_args()

    # =================================================================
    # get grids from context input file
    with open(root_directory + "context_input.txt", "r") as f:
        input_file_lines = f.readlines()
    # remove comments and empty lines, and carriage returns
    input_file_lines = list(
        numpy.array(
            [
                l.replace("\n", "").strip()
                for l in input_file_lines
                if l[0] != "#" and l != "\n"
            ]
        )
    )

    grid_file = readparam(input_file_lines, ["grid_file"])
    labels_file = [
        grid_directory + "list_observables.dat",
    ]  # convenient list of keys
    params_file = [
        grid_directory + "list_parameters.dat",
    ]  # convenient list of keys
    files = [
        grid_directory + grid_file,
    ]

    post_processing_file = readparam(input_file_lines, ["post_processing_file"])
    if post_processing_file is not None:
        post_processing_file = post_processing_file[0]
        labels_file += [
            grid_directory + "list_post-processing.dat",
        ]
        files += [
            grid_directory + post_processing_file,
        ]

    interp8 = True  # interpolate lum=8 as pre-processing to extend table?

    # =================================================================
    # pre-process and convert

    for i, f in enumerate(
        files
    ):  # 0 is primary parameters + tracers, 1 is secondary parameters
        print("\n------------------------------------")

        # keeping all columns...
        columns = None
        # ...or keeping only some columns for memory issues
        # columns = readparam(input_file_lines, ['primary_parameters'])+['Stop_Av10'] #mandatory
        # columns += ['Al22660.35A',] #some tracers
        data = read_data(
            f,  # will read either .csv or .fth files
            columns=columns,
            delimiter=readparam(input_file_lines, ["delimiter"]),  # if ascii
            chunk=args.chunks,
        )

        data = data.astype("float32")

        # -------------------------------------------------------------------
        print("Making some changes...")
        # UPDATE

        # rename/clean column labels
        # data = data.rename(columns={tmp: tmp.replace(' ', '').replace("'", '').replace('(', '').replace(')', '') for tmp in data.keys()})

        msol = 30.2986347831

        # change Z scale?
        # data['Zsun'] = data['Z'] +(12-8.69)

        # the SFGX Cloudy model table uses the ISM table with O/H=8.50 but here we would like to get the metallicity wrt solar
        # data["Z"] += 8.50 - 8.69
        data["Z"] = (
            np.round((np.array(data["Z"]) + 8.50 - 8.69) * 1000) / 1000
        )  # keep precision from table

        print("- Checking duplicates...")
        data = check_duplicates(
            data, columns=readparam(input_file_lines, ["primary_parameters"])
        )

        print("- Dropping some incomplete models...")
        # experimental: drop rows for which cut=4 doesn't correspond to Av=10 (thus interpolation between cut=2 and 3 is not reliable)
        data.drop(
            data.loc[(data["cut"] == 4) & (data["Stop_Av10"] == 0)].index, inplace=True
        )
        data.reset_index(inplace=True, drop=True)

        # breakpoint()
        # tt = data
        # tt.loc[(tt['lum']==7)*(tt['n']==0)*(tt['u']==0)*(tt['Z']==0.111)*(tt['age']==0)*(tt['Lx']==-40)][['cut', 'fescL(1-infty)', 'fesc(1-infty)', 'Q0', 'Q(1-infty)']]

        print("- Number of models: ", len(data))

        # observables & primary parameters
        if i == 0:  # only for main grid
            print("- Primary grid")

            # do this before interpolation
            print("...Temporary: replacing NaNs by -inf")
            data.fillna(-np.inf, inplace=True)
            # per column is better for memory purposes but it's loooong
            # for col in tqdm(data.columns):
            #    data[col] = data[col].fillna(-np.inf)
            # needs to be consistent with line mask_ul_grid[i][np.isinf(obs_grid[i])] = 0 in Library/lib_main.py (make_obs_grid_wrapper)

            if interp8:
                print("Interpolating L=8 values...")
                params = readparam(input_file_lines, ["primary_parameters"])
                observable_list = tuple(  # tuple for pandas .at
                    [
                        d
                        for d in data.keys()
                        if d not in params
                        and d not in ("index", "#model_number", "Stop_Av10")
                    ]
                )
                ref = pd.merge(
                    data.astype(np.float32).loc[data["lum"] == 7],
                    data.astype(np.float32).loc[data["lum"] == 9],
                    on=list(set(params) - {"lum"}),
                    how="left",
                    suffixes=["_7", ""],
                )
                tmp = deepcopy(data.loc[data["lum"] == 7])
                tmp["lum"] = 8
                for o in tqdm(observable_list):
                    tmp.loc[:, o] = 0.5 * (
                        data.loc[data["lum"] == 7, o].reset_index()
                        + ref.loc[:, o].reset_index()
                    )
                # tmp = deepcopy(data.loc[data["lum"] == 7])
                # tmp["lum"] = 8
                # tmp.at[:, observable_list] = 0.5 * (
                #     data.loc[data["lum"] == 7, observable_list].reset_index()
                #     + ref.loc[:, observable_list].reset_index()
                # )
                del ref
                # data = data.append(tmp, ignore_index=True)
                data = pd.concat([data, tmp], ignore_index=True)
                del tmp
                data["index"] = data.index
                data["#model_number"] = data.index.astype(float)

            print("...Creating some sums & ratios")
            data["O16300+63A"] = logsum_simple([data["O16300.30A"], data["O16363.78A"]])
            data["O23726+9A"] = logsum_simple([data["O23726.03A"], data["O23728.81A"]])
            data["O27320+30A"] = logsum_simple([data["O27319.99A"], data["O27330.73A"]])
            data["O35007+4959A"] = logsum_simple(
                [data["O35006.84A"], data["O34958.91A"]]
            )
            data["rO3"] = (
                data["O34363.21A"] - data["O35007+4959A"]
            )  # this is an intensive obs to we may need to give a meaning to -inf (it is not an upper limit)
            data["rO3"] = data["rO3"].replace([-np.inf, np.nan], -100)
            data["S39069+532A"] = logsum_simple(
                [data["S39068.62A"], data["S39530.62A"]]
            )
            data["S26716+30A"] = logsum_simple([data["S26716.44A"], data["S26730.82A"]])
            data["Ar44711+40A"] = logsum_simple(
                [data["Ar44711.26A"], data["Ar44740.12A"]]
            )
            data["N26548+84A"] = logsum_simple([data["N26548.05A"], data["N26583.45A"]])
            data["Ar37135+751A"] = logsum_simple(
                [data["Ar37135.79A"], data["Ar37751.11A"]]
            )
            data["PAH6-15um"] = logsum_simple(
                [
                    data["PAH6.20000m"],
                    data["PAH7.90000m"],
                    data["PAH11.3000m"],
                    data["PAH11.8000m"],
                    data["PAH13.3000m"],
                ]
            )  # remove continuum? PAHC5.65000m-PAHC14.1000m
            data["S26716+30A"] = logsum_simple([data["S26716.44A"], data["S26730.82A"]])
            data["S26716/30A"] = data["S26716.44A"] - data["S26730.82A"]
            data["Ne33869+968A"] = logsum_simple(
                [data["Ne33868.76A"], data["Ne33967.47A"]]
            )

            # observables not in the main table will be also looked for in the post-processing table
            # but you can always include here some parameters from the post-processing table, e.g., to use as observable IF rows are exactly in the same order in both the main table and the post-processing table
            # data['MHI'] = pd.read_csv(files[1], sep=readparam(input_file_lines, ['delimiter']))['MHI'] - msol
            # data['fesc(1-infty)'] = pd.read_csv(files[1], sep=readparam(input_file_lines, ['delimiter']))['fesc(1-infty)']

            print("   Checking duplicates...")
            # final duplicate check: if primary parameters have been modified, check that we have only single sets
            data = check_duplicates(
                data, columns=readparam(input_file_lines, ["primary_parameters"])
            )

            ffile = grid_directory + "/model_grid.fth"

            with open(params_file[i], "w") as of:
                print("===========================")
                for p in readparam(input_file_lines, ["primary_parameters"]):
                    print("Parameter", p)
                    of.write("\n\nPrimary parameter: {}\n".format(p))
                    of.write(str(sorted(set(data[p].values))))
                    print(str(sorted(set(data[p].values))))
                    print("===========================")
            print("Writing list of parameters in {}".format(params_file[i]))

        # secondary parameters
        elif i == 1:  # only for post-processing grid
            print("- Secondary grid:")

            if "model_number" in data.keys():
                data["#model_number"] = data["model_number"]
                data.drop("model_number", axis=1, inplace=True)

            # do this before interpolation
            print("...Temporary: replacing NaNs by -inf")
            # this is for *observables* to be predicted
            data.fillna(-np.inf, inplace=True)
            # needs to be consistent with line mask_ul_grid[i][np.isinf(obs_grid[i])] = 0 in Library/lib_main.py (make_obs_grid_wrapper)

            if interp8:
                print("Interpolating L=8 values...")
                params = readparam(input_file_lines, ["primary_parameters"])
                observable_list = [
                    d
                    for d in data.keys()
                    if d not in params
                    and d not in ("index", "#model_number", "Stop_Av10")
                ]
                ref = pd.merge(
                    data.astype(np.float32).loc[data["lum"] == 7],
                    data.astype(np.float32).loc[data["lum"] == 9],
                    on=list(set(params) - {"lum"}),
                    how="left",
                    suffixes=["_7", ""],
                )
                tmp = deepcopy(data.loc[data["lum"] == 7])
                tmp["lum"] = 8
                for o in tqdm(observable_list):
                    tmp.loc[:, o] = 0.5 * (
                        data.loc[data["lum"] == 7, o].reset_index()
                        + ref.loc[:, o].reset_index()
                    )
                # tmp.at[:, observable_list] = 0.5 * (
                #     data.loc[data["lum"] == 7, observable_list].reset_index()
                #     + ref.loc[:, observable_list].reset_index()
                # )
                del ref
                # data = data.append(tmp, ignore_index=True)
                data = pd.concat([data, tmp], ignore_index=True)
                del tmp
                data["index"] = data.index
                data["#model_number"] = data.index.astype(float)

            print("...Changing units")
            data["MH2_total"] = data["MH2_total"] - msol
            data["MH2_CO"] = data["MH2_CO"] - msol
            data["MH2_C"] = data["MH2_C"] - msol
            data["MH2_C+"] = data["MH2_C+"] - msol
            data["MHI"] = data["MHI"] - msol
            data["MHII"] = data["MHII"] - msol
            data["Mdust"] = data["Mdust"] - msol
            data["Lx_sun"] = data["Lx"] + data["lum"]
            # data['alpha_CO'] = data['MH2'] - np.log10(4000) + np.log10(0.18) - np.log10(data['CO2600.05m'])

            # calculate all fesc values
            try:
                print("...Calculating all fesc values...")
                # for erange in ('1-infty', '1-1.8', '1.8-4', '4-20', '20-infty'):
                #     data.drop(f'fesc({erange})', axis=1, inplace=True)
                #     data[f'fesc({erange})'] = np.nan
                tmp = deepcopy(data)
                params = readparam(input_file_lines, ["primary_parameters"])
                tmp = tmp[params]
                tmp["cut"] = 0
                ref = pd.merge(
                    tmp.astype(np.float32),
                    data.astype(np.float32),
                    on=params,
                    how="left",
                )

                for erange in ("1-infty", "1-1.8", "1.8-4", "4-20", "20-infty"):
                    print(erange)

                    fescstrL = f"fescL({erange})"
                    data[fescstrL] = data[f"L({erange})"] - ref[f"L({erange})"]
                    # forcing max of 0 (in principle this is only due to rounding errors)
                    n = len(data.loc[data[fescstrL] > 0])
                    if n > 0:
                        print(f"  - /!\ {n} fescL values are >0 in log")
                        print(
                            f"  - /!\ Maximum fescL: {data[np.isfinite(data[fescstrL])][fescstrL].max()}"
                        )
                    data.loc[data[fescstrL] > 0, fescstrL] = 0

                    fescstr = f"fesc({erange})"
                    data[fescstr] = data[f"Q({erange})"] - ref[f"Q({erange})"]
                    # forcing max of 0 (in principle this is only due to rounding errors)
                    n = len(data.loc[data[fescstr] > 0])
                    if n > 0:
                        print(f"  - /!\ {n} fesc values are >0 in log")
                        print(
                            f"  - /!\ Maximum fesc: {data[np.isfinite(data[fescstrL])][fescstr].max()}"
                        )
                    data.loc[data[fescstr] > 0, fescstr] = 0
            except:
                print("Old grid")

            # with a loop
            # for idx in range(len(data)):
            #     if data.iloc[idx]['fesc(1-infty)']
            #     # get the corresponding row wth cut=0
            #     tmp.at[idx,'cut'] = 0
            #     # now we get the corresponding in data
            #     refrow = data.loc[((data[params]==tmp.loc[idx]) + (np.isnan(data[params])*np.isnan(tmp.loc[idx]))).all(axis=1)] #either the values are = or both are NaNs
            #     for erange in ('1-infty', '1-1.8', '1.8-4', '4-20', '20-infty'):
            #         data.at[idx, f'fesc({erange})'] = data.at[idx, f'Q({erange})'] - refrow[f'Q({erange})']

            ffile = grid_directory + "/model_grid_post_processing.fth"

        # keep a convenient (full) list of observables
        observable_list = [
            d + "\n"
            for d in data.keys()
            if d not in readparam(input_file_lines, ["primary_parameters"])
        ]
        with open(labels_file[i], "w") as of:
            for o in observable_list:
                of.write(o)
        print("...Writing list of observables in {}".format(labels_file[i]))

        # -------------------------------------------------------------------
        # change to luminosity?
        # [np.log10(np.power(10,values[list_of_lines[j]][ind[0]])/(4*np.pi*(distance*1e6*pc2cm)**2)) for j in range(n_lines)]

        # this is to avoid appending
        # ffile = os.path.splitext(f)[0]+'.fth'
        if os.path.exists(ffile):
            os.remove(ffile)

        print("...Writing new Feather file... {}".format(ffile))
        data.to_feather(ffile)
        del data

    print("------------------------------------")
    print("Dropping all nans/infs rows")

    data = pd.read_feather(grid_directory + "/model_grid.fth")
    datapp = pd.read_feather(grid_directory + "/model_grid_post_processing.fth")

    observable_list = [
        d
        for d in data.keys()
        if d not in readparam(input_file_lines, ["primary_parameters"])
        and d not in ("#model_number", "index")
    ]

    todrop = list(data.loc[~numpy.isfinite(datapp["Q0"])].index)
    todrop += list(
        data.loc[
            numpy.isfinite(data["H16562.81A"]) & ~numpy.isfinite(datapp["depth_stop"])
        ].index
    )
    todrop += list(data.loc[data[observable_list].max(axis=1) < 2].index)

    data.drop(todrop, inplace=True)
    datapp.drop(todrop, inplace=True)

    ffile = grid_directory + "/model_grid.fth"
    print("...Writing new Feather file... {}".format(ffile))
    data.reset_index(inplace=True, drop=True)
    data.to_feather(ffile)

    ffile = grid_directory + "/model_grid_post_processing.fth"
    print("...Writing new Feather file... {}".format(ffile))
    datapp.reset_index(inplace=True, drop=True)
    datapp.to_feather(ffile)

    print("------------------------------------")


# --------------------------------------------------------------------
if __name__ == "__main__":
    main()
