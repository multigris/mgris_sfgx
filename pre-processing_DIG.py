# This is when running the script from the root mgris directory
import sys, os

s = os.path.dirname(os.path.abspath(__file__))
if "Contexts" in s:  # add root
    s = s[: s.index("Contexts/")]
    sys.path.insert(0, s)

import argparse
import numpy as np
import pandas as pd
from Library.lib_mc import logsum_simple
from Library.lib_main import readparam, read_data, check_duplicates
from Library.lib_misc import FileExists, inputRL

# import dask.dataframe as dd

from copy import deepcopy
from tqdm import tqdm

# =================================================================
def main(args=None):

    root_directory = os.path.dirname(os.path.abspath(__file__)) + "/"
    grid_directory = root_directory + "/Grids/"

    if args is None:
        parser = argparse.ArgumentParser(description="MULTIGRIS pre-processing step")
        parser.add_argument(
            "--chunks", "-c", help="Read as chunks", action="store_true"
        )
    args = parser.parse_args()

    # =================================================================
    # get grids from context input file
    with open(root_directory + "context_input.txt", "r") as f:
        input_file_lines = f.readlines()
    # remove comments and empty lines, and carriage returns
    input_file_lines = list(
        np.array(
            [
                l.replace("\n", "").strip()
                for l in input_file_lines
                if l[0] != "#" and l != "\n"
            ]
        )
    )

    grid_file = readparam(input_file_lines, ["grid_file"])
    labels_file = [
        grid_directory + "list_observables.dat",
    ]  # convenient list of keys
    params_file = [
        grid_directory + "list_parameters.dat",
    ]  # convenient list of keys
    files = [
        grid_directory + grid_file,
    ]

    post_processing_file = readparam(input_file_lines, ["post_processing_file"])
    if post_processing_file is not None:
        post_processing_file = post_processing_file[0]
        labels_file += [
            grid_directory + "list_post-processing.dat",
        ]
        files += [
            grid_directory + post_processing_file,
        ]

    interp8 = True  # interpolate lum=8 as pre-processing to extend table?

    # =================================================================
    # pre-process and convert

    for i, f in enumerate(
        files
    ):  # 0 is primary parameters + tracers, 1 is secondary parameters

        print("\n------------------------------------")

        # keeping all columns...
        columns = None
        # ...or keeping only some columns for memory issues
        # columns = readparam(input_file_lines, ['primary_parameters'])+['Stop_Av10'] #mandatory
        # columns += ['Al22660.35A',] #some tracers
        data = read_data(
            f,  # will read either .csv or .fth files
            columns=columns,
            delimiter=readparam(input_file_lines, ["delimiter"]),  # if ascii
            chunk=args.chunks,
        )
        # -------------------------------------------------------------------
        print("Making some changes...")
        # UPDATE

        data = check_duplicates(data)

        # rename/clean column labels
        # data = data.rename(columns={tmp: tmp.replace(' ', '').replace("'", '').replace('(', '').replace(')', '') for tmp in data.keys()})

        msol = 30.2986347831

        # change Z scale?
        # data['Zsun'] = data['Z'] +(12-8.69)

        # the SFGX Cloudy model table uses the ISM table with O/H=8.50 but here we would like to get the metallicity wrt solar
        data["Z"] += 8.50 - 8.69
        data["Z"] = round(data["Z"] * 1000) / 1000  # keep precision from table

        # experimental: drop rows for which cut=4 doesn't correspond to Av=10 (thus interpolation between cut=2 and 3 is not reliable)
        data = data.drop(data.loc[(data["cut"] == 4) & (data["Stop_Av10"] == 0)].index)
        data = data.reset_index()

        print("Number of models: ", len(data))

        # observables & primary parameters
        if i == 0:  # only for main grid
            print("- Primary grid")

            # pp_table = pd.read_feather(files[1])
            pp_table = read_data(files[1], chunk=args.chunks)
            pp_table = pp_table.drop(
                pp_table.loc[
                    (pp_table["cut"] == 4) & (pp_table["Stop_Av10"] == 0)
                ].index
            )
            pp_table = pp_table.reset_index()
            assert len(data) == len(pp_table)
            data["U_mean_vol"] = np.round(pp_table["U_mean_vol"], 0)

            # that's the U for each cut
            data["uDIG"] = np.round(  # rounding because then the grid is regularized
                pp_table["Q(1-infty)"]
                - np.log10(4.0 * np.pi * 2.99792458e10)
                # - data["n"]
                - 0
                - 2 * np.log10(10 ** pp_table["Rin"] + 10 ** pp_table["depth_stop"])
            )
            # set() doesn't do well with nans (nans because of Q(1-infty))
            data["uDIG"].clip(data["u"].min(), data["u"].max(), inplace=True)
            data["uDIG"].fillna(data["u"].min(), inplace=True)
            tmp_udig = data["uDIG"]

            from scipy.interpolate import interp1d

            data["ageDIG"] = data["age"].max()
            data["Q0"] = pp_table["Q0"]
            data["Q(1-infty)"] = pp_table["Q(1-infty)"]
            params = readparam(input_file_lines, ["primary_parameters"])
            tmp = data[params + ["Q0", "Q(1-infty)"]]  # no need to take full table
            params = [p for p in params if p not in ("age", "ageDIG", "uDIG")]
            # models with lum, age, Lx, Tx, Z have same Q0
            # n, u, q_PAH, Z_dust, cut don't matter
            # cycle through lum, Z, Lx, Tx and age values?
            for lum in tqdm(set(data["lum"])):
                for Z in set(data["Z"]):
                    for Lx in set(data["Lx"]):
                        for Tx in set(data["Tx"]):
                            x = tmp.loc[
                                (tmp["lum"] == lum)
                                & (tmp["Z"] == Z)
                                & (tmp["Lx"] == Lx)
                                & (tmp["Tx"] == Tx)
                            ]  # only age will control Q0
                            if len(x) == 0:
                                continue  # e.g., for Lx -40 and Lx>0
                            q = sorted(np.array(list(set(x["Q0"].dropna()))))
                            a = sorted(np.array(list(set(x["age"].dropna()))))[::-1]
                            if len(q) != len(a):
                                continue
                            x["ageDIG"] = interp1d(
                                q, a, kind="nearest", fill_value="extrapolate"
                            )(x["Q(1-infty)"])
                            tmp.loc[
                                (tmp["lum"] == lum)
                                & (tmp["Z"] == Z)
                                & (tmp["Lx"] == Lx)
                                & (tmp["Tx"] == Tx),
                                "ageDIG",
                            ] = x["ageDIG"]
            data["ageDIG"] = tmp["ageDIG"]
            data["ageDIG"].clip(data["age"].min(), data["age"].max(), inplace=True)
            data["ageDIG"].fillna(data["age"].max(), inplace=True)
            tmp_agedig = data["ageDIG"]

            data["Q0"] = pp_table["Q0"]
            data["Q0"].clip(1, 55, inplace=True)
            data["Q0"].fillna(1, inplace=True)
            data["Qcut"] = pp_table["Q(1-infty)"]
            data["Qcut"].clip(1, 55, inplace=True)
            data["Qcut"].fillna(1, inplace=True)

            if interp8:
                print("Interpolating L=8 values...")
                params = readparam(input_file_lines, ["primary_parameters"])
                observable_list = tuple(  # tuple for pandas .at
                    [
                        d
                        for d in data.keys()
                        if d not in params
                        and d not in ("index", "#model_number", "Stop_Av10")
                    ]
                )
                ref = pd.merge(
                    data.astype(np.float32).loc[data["lum"] == 7],
                    data.astype(np.float32).loc[data["lum"] == 9],
                    on=list(set(params) - {"lum"}),
                    how="left",
                    suffixes=["_7", ""],
                )
                tmp = deepcopy(data.loc[data["lum"] == 7])
                tmp["lum"] = 8
                for o in tqdm(observable_list):
                    tmp.loc[:, o] = 0.5 * (
                        data.loc[data["lum"] == 7, o].reset_index()
                        + ref.loc[:, o].reset_index()
                    )
                # tmp = deepcopy(data.loc[data["lum"] == 7])
                # tmp["lum"] = 8
                # tmp.at[:, observable_list] = 0.5 * (
                #     data.loc[data["lum"] == 7, observable_list].reset_index()
                #     + ref.loc[:, observable_list].reset_index()
                # )
                del ref
                data = data.append(tmp, ignore_index=True)
                del tmp
                data["index"] = data.index
                data["#model_number"] = data.index.astype(float)

            print("...Creating some sums & ratios")
            data["O16300+63A"] = logsum_simple([data["O16300.30A"], data["O16363.78A"]])
            data["O23726+9A"] = logsum_simple([data["O23726.03A"], data["O23728.81A"]])
            data["O27320+30A"] = logsum_simple([data["O27319.99A"], data["O27330.73A"]])
            data["O35007+4959A"] = logsum_simple(
                [data["O35006.84A"], data["O34958.91A"]]
            )
            data["rO3"] = (
                data["O34363.21A"] - data["O35007+4959A"]
            )  # this is an intensive obs to we may need to give a meaning to -inf (it is not an upper limit)
            data["rO3"] = data["rO3"].replace([-np.inf, np.nan], -100)
            data["S39069+532A"] = logsum_simple(
                [data["S39068.62A"], data["S39530.62A"]]
            )
            data["S26716+30A"] = logsum_simple([data["S26716.44A"], data["S26730.82A"]])
            data["Ar44711+40A"] = logsum_simple(
                [data["Ar44711.26A"], data["Ar44740.12A"]]
            )
            data["N26548+84A"] = logsum_simple([data["N26548.05A"], data["N26583.45A"]])
            data["Ar37135+751A"] = logsum_simple(
                [data["Ar37135.79A"], data["Ar37751.11A"]]
            )
            data["PAH6-15um"] = logsum_simple(
                [
                    data["PAH6.20000m"],
                    data["PAH7.90000m"],
                    data["PAH11.3000m"],
                    data["PAH11.8000m"],
                    data["PAH13.3000m"],
                ]
            )  # remove continuum? PAHC5.65000m-PAHC14.1000m
            data["S26716+30A"] = logsum_simple([data["S26716.44A"], data["S26730.82A"]])
            data["S26716/30A"] = data["S26716.44A"] - data["S26730.82A"]
            data["Ne33869+968A"] = logsum_simple(
                [data["Ne33868.76A"], data["Ne33967.47A"]]
            )

            # print("...Temporary: replacing NaNs by -inf")
            data.fillna(-np.inf, inplace=True)
            # needs to be consistent with line mask_ul_grid[i][np.isinf(obs_grid[i])] = 0 in Library/lib_main.py (make_obs_grid_wrapper)

            # observables not in the main table will be also looked for in the post-processing table
            # but you can always include here some parameters from the post-processing table, e.g., to use as observable IF rows are exactly in the same order in both the main table and the post-processing table
            # data['MHI'] = pd.read_csv(files[1], sep=readparam(input_file_lines, ['delimiter']))['MHI'] - msol
            # data['fesc(1-infty)'] = pd.read_csv(files[1], sep=readparam(input_file_lines, ['delimiter']))['fesc(1-infty)']

            # final duplicate check: if primary parameters have been modified, check that we have only single sets
            data = check_duplicates(
                data, columns=readparam(input_file_lines, ["primary_parameters"])
            )

            ffile = grid_directory + "/model_grid.fth"

            with open(params_file[i], "w") as of:
                for p in readparam(input_file_lines, ["primary_parameters"]):
                    of.write("\n\nPrimary parameter: {}\n".format(p))
                    of.write(str(sorted(set(data[p].values))))
            print("...Writing list of parameters in {}".format(params_file[i]))

        # secondary parameters
        elif i == 1:  # only for post-processing grid
            print("- Secondary grid:")

            if "model_number" in data.keys():
                data["#model_number"] = data["model_number"]
                data.drop("model_number", axis=1, inplace=True)

            data["uDIG"] = tmp_udig
            data["ageDIG"] = tmp_agedig

            if interp8:
                print("Interpolating L=8 values...")
                params = readparam(input_file_lines, ["primary_parameters"])
                observable_list = [
                    d
                    for d in data.keys()
                    if d not in params
                    and d not in ("index", "#model_number", "Stop_Av10")
                ]
                ref = pd.merge(
                    data.astype(np.float32).loc[data["lum"] == 7],
                    data.astype(np.float32).loc[data["lum"] == 9],
                    on=list(set(params) - {"lum"}),
                    how="left",
                    suffixes=["_7", ""],
                )
                tmp = deepcopy(data.loc[data["lum"] == 7])
                tmp["lum"] = 8
                for o in tqdm(observable_list):
                    tmp.loc[:, o] = 0.5 * (
                        data.loc[data["lum"] == 7, o].reset_index()
                        + ref.loc[:, o].reset_index()
                    )
                # tmp.at[:, observable_list] = 0.5 * (
                #     data.loc[data["lum"] == 7, observable_list].reset_index()
                #     + ref.loc[:, observable_list].reset_index()
                # )
                del ref
                data = data.append(tmp, ignore_index=True)
                del tmp
                data["index"] = data.index
                data["#model_number"] = data.index.astype(float)

            # print('...Temporary: replacing NaNs by -inf')
            # this is for *observables* to be predicted
            data.fillna(-np.inf, inplace=True)
            # needs to be consistent with line mask_ul_grid[i][np.isinf(obs_grid[i])] = 0 in Library/lib_main.py (make_obs_grid_wrapper)

            print("...Changing units")
            data["MH2_total"] = data["MH2_total"] - msol
            data["MH2_CO"] = data["MH2_CO"] - msol
            data["MH2_C"] = data["MH2_C"] - msol
            data["MH2_C+"] = data["MH2_C+"] - msol
            data["MHI"] = data["MHI"] - msol
            data["MHII"] = data["MHII"] - msol
            data["Mdust"] = data["Mdust"] - msol
            data["Lx_sun"] = data["Lx"] + data["lum"]
            # data['alpha_CO'] = data['MH2'] - np.log10(4000) + np.log10(0.18) - np.log10(data['CO2600.05m'])

            # calculate all fesc values
            try:
                print("...Calculating all fesc values...")
                # for erange in ('1-infty', '1-1.8', '1.8-4', '4-20', '20-infty'):
                #     data.drop(f'fesc({erange})', axis=1, inplace=True)
                #     data[f'fesc({erange})'] = np.nan
                tmp = deepcopy(data)
                params = readparam(input_file_lines, ["primary_parameters"])
                tmp = tmp[params]
                tmp["cut"] = 0
                ref = pd.merge(
                    tmp.astype(np.float32),
                    data.astype(np.float32),
                    on=params,
                    how="left",
                )

                for erange in ("1-infty", "1-1.8", "1.8-4", "4-20", "20-infty"):
                    print(erange)

                    fescstrL = f"fescL({erange})"
                    data[fescstrL] = data[f"L({erange})"] - ref[f"L({erange})"]
                    # forcing max of 0 (in principle this is only due to rounding errors)
                    n = len(data.loc[data[fescstrL] > 0])
                    if n > 0:
                        print(f"  - /!\ {n} fescL values are >0 in log")
                        print(f"  - /!\ Maximum fescL: {data[fescstrL].max()}")
                    data.loc[data[fescstrL] > 0, fescstrL] = 0

                    fescstr = f"fesc({erange})"
                    data[fescstr] = data[f"Q({erange})"] - ref[f"Q({erange})"]
                    # forcing max of 0 (in principle this is only due to rounding errors)
                    n = len(data.loc[data[fescstr] > 0])
                    if n > 0:
                        print(f"  - /!\ {n} fesc values are >0 in log")
                        print(f"  - /!\ Maximum fesc: {data[fescstr].max()}")
                    data.loc[data[fescstr] > 0, fescstr] = 0
            except:
                print("Old grid")

            # with a loop
            # for idx in range(len(data)):
            #     if data.iloc[idx]['fesc(1-infty)']
            #     # get the corresponding row wth cut=0
            #     tmp.at[idx,'cut'] = 0
            #     # now we get the corresponding in data
            #     refrow = data.loc[((data[params]==tmp.loc[idx]) + (np.isnan(data[params])*np.isnan(tmp.loc[idx]))).all(axis=1)] #either the values are = or both are NaNs
            #     for erange in ('1-infty', '1-1.8', '1.8-4', '4-20', '20-infty'):
            #         data.at[idx, f'fesc({erange})'] = data.at[idx, f'Q({erange})'] - refrow[f'Q({erange})']

            ffile = grid_directory + "/model_grid_post_processing.fth"

        # keep a convenient (full) list of observables
        observable_list = [
            d + "\n"
            for d in data.keys()
            if d not in readparam(input_file_lines, ["primary_parameters"])
        ]
        with open(labels_file[i], "w") as of:
            for o in observable_list:
                of.write(o)
        print("...Writing list of observables in {}".format(labels_file[i]))

        # -------------------------------------------------------------------
        # change to luminosity?
        # [np.log10(np.power(10,values[list_of_lines[j]][ind[0]])/(4*np.pi*(distance*1e6*pc2cm)**2)) for j in range(n_lines)]

        # this is to avoid appending
        # ffile = os.path.splitext(f)[0]+'.fth'
        if os.path.exists(ffile):
            os.remove(ffile)

        print("...Writing new Feather file... {}".format(ffile))
        data.to_feather(ffile)

    print("------------------------------------")
    # temporary: nan for tracers that are valid while pp parameters are not (when Av=10 is reached?)
    print(
        "- Temporary fix: ignoring primary tracers for which secondary parameters (depth_stop used as reference) are NaN"
    )
    data = pd.read_feather(grid_directory + "/model_grid.fth")
    datapp = pd.read_feather(grid_directory + "/model_grid_post_processing.fth")
    observable_list = [
        d
        for d in data.keys()
        if d not in readparam(input_file_lines, ["primary_parameters"])
        and d not in ("#model_number", "index")
    ]
    print(
        "...Number of rows: ",
        len(
            data.loc[
                np.isfinite(data["H16562.81A"]) & ~np.isfinite(datapp["depth_stop"]),
                observable_list,
            ]
        ),
    )
    data.loc[
        np.isfinite(data["H16562.81A"]) & ~np.isfinite(datapp["depth_stop"]),
        observable_list,
    ] *= np.nan

    ffile = grid_directory + "/model_grid.fth"
    print("...Writing new Feather file... {}".format(ffile))
    data.to_feather(ffile)

    print("------------------------------------")


# --------------------------------------------------------------------
if __name__ == "__main__":

    main()
