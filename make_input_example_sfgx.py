import importlib

try:
    import Library.lib_input
except:
    print("Make sure to setup the pythonpath environment variable, see INSTALL.")
    exit()

importlib.reload(Library.lib_input)
from Library.lib_input import *

# Retrieve list of known input parameters

params = get_params()

# ------------------------------------------------
# output directory (absolute or relative from the folder the mgris code is launched)

params["output"].value = "Results_example_sfgx/"

# ------------------------------------------------
# Setting a context

params["context"].value = "Contexts/mgris_sfgx"

# ------------------------------------------------
# Setting a configuration

# Setting a pre-defined configuration?
params["USE configuration"].value = "1C1S"

# Setting some hard constraints
params["select"].value = [
    "cut [3,3]",  # i.e., force cut=3
    "Z_dust [1,2]",  # i.e., Z_dust between 1 and 2
]  # possible to have several range commands
params["select"].comment()

# Setting some specific priors to complement/replace pre-defined configuration
# all distributions are assumed to be single (i.e., not described as power-law, normal etc...) and with nearest neighbor interpolation
params[
    "BEGIN configuration"
].value = """n_comps 2
age (0) < 0.2
distrib Z single linear
Z (0) = -1 0.1
Lx (1) < (0)
Tx (*) = 
"""
params["BEGIN configuration"].comment()

# ------------------------------------------------
# Systematic uncertainties

params["sysunc"].value = [
    "elemental_abundances 0.2 ['O351.8004m', 'O1145.495m', 'O163.1679m', 'O388.3323m']",
    "irs 0.2 ['Si234.8046m', 'S333.4704m', 'Ne524.2065m', 'Ne212.8101m', 'Ne315.5509m', 'Ne514.3228m']",
]
params["sysunc"].comment()

# ------------------------------------------------
# List of observables

labels = [
    "C2157.636m",
    "O163.1679m",
    "O388.3323m",
    "N2205.244m",
    "H217.0300m",
    "Si234.8046m",
    "Ne212.8101m",
    "Ne315.5509m",
    "Ne514.3228m",
]
values = [
    10**41.044147,
    10**40.432007,
    10**37.74601,
    10**38.80801,
    10**40.259117,
    10**38.83803,
    10**39.262924,
    10**38.241047,
    10**25.899984,
]
# 10% errors
errors = [v * 0.1 for v in values]

params["BEGIN observations"].value = ""
for i, l in enumerate(labels):
    params["BEGIN observations"].value += "{} {} {}\n".format(l, values[i], errors[i])
params["BEGIN observations"].extras = {
    "scale": "linear",
    "scale_factor": 1,
    "delta_add": 0.1,
}
# ------------------------------------------------
# Use observation sets instead (i.e., several groups of observations)?

# params['observation_sets'] = [InputParameter('BEGIN observations'),
#                               InputParameter('BEGIN observations'), ]

# params['observation_sets'][0].value = ''
# for i,l in enumerate(labels):
#     params['observation_sets'][0].value += '{} {}\n'.format(l, values[i])
# params['observation_sets'][0].extras = {'delta_ref': 0.1, 'scale': 'log', 'scale_factor': 10, 'name': 'setobs1'}

# params['observation_sets'][1].value = ''
# for i,l in enumerate(labels):
#     params['observation_sets'][1].value += '{} {}\n'.format(l, values[i])
# params['observation_sets'][1].extras = {'delta_ref': 0.1, 'scale': 'log', 'scale_factor': 0, 'name': 'setobs2'}

# ------------------------------------------------
# Which observable do we use as constraint for the prior on the scaling parameter?

params["use_scaling"].value = "'all'"
# params['use_scaling'].value = "'H__1_486133A'"

# ------------------------------------------------
# Predict some unobserved quantities?

params["obs_to_predict"].value = [
    "H16562.81A",
    "N2121.767m",
    "O351.8004m",
    "Ne524.2065m",
    "O1145.495m",
    "S333.4704m",
    "Ne53426.03A",
]

# Predict some secondary parameters?
params["secondary_parameters"].value = ["fesc(Ha)"]
params["secondary_parameters"].comment()

# ------------------------------------------------
# Activate some context-specific rules

params["active_rules"].value = ["extinction"]
params["active_rules"].comment()

# ------------------------------------------------
# Global parameters can be overridden

params["nsamples_per_chain_max"].value = 100000
params["nsamples_per_chain_max"].comment()

# ---------------------------------------
# Write the resulting input file

make_input(params, "input_example_sfgx.txt")
